package vi_zad2v1;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.ArrayList;

public class UserScoreMap {
	public TreeMap<Double, TreeMap<String, Double>> mapByScore = new TreeMap<Double, TreeMap<String, Double>>();
	public TreeMap<String, Double> mapByCookie = new TreeMap<String, Double>();
	
	public TreeMap<String, Double> get(Double key){
		return mapByScore.get(key);
	}
	/*
	public String get(Double key, String s){
		return mapByScore.get(key).get(s);
	}
	*/
	public Double get(String key){
		return mapByCookie.get(key);
	}
	public void put(Double i, String s){
		TreeMap<String, Double> al; 
		//if(mapBySmeId.get(i))
		//mapByScore.remove(i);
		//mapByCookie.remove(s);
		
		//mapByScore.put(i, s);
		System.out.println("i pod n: "+i);
		if(mapByScore.get(i)==null){
			al = new TreeMap<String, Double>();
			al.put(s, i);
			mapByScore.put(i, al);
		}
		else{
			mapByScore.get(i).put(s, i);
		}
		mapByCookie.put(s, i);
	}
	
	public void put(String s, Double i){
		//if(mapBySmeId.get(i))
		TreeMap<String, Double> al; 
		//mapByScore.remove(i);
		//mapByCookie.remove(s);
		mapByCookie.put(s, i);
		if(mapByScore.get(i)==null){
			
			al = new TreeMap<String, Double>();
			al.put(s, i);
			//System.out.println("Vytvara sa nova hodnota score: "+i+" ("+s+")");
			mapByScore.put(i, al);
		}
		else{
			if(i!=0.0)
				System.out.println("Existujuce score: "+i+" ("+s+") size: "+mapByScore.get(i).size());
			mapByScore.get(i).put(s, i);
		}
		//mapByScore.put(i, s);
		//System.out.println("Tu vkladam i: "+i+" -- "+mapByScore.get(i).toString());
	}
	
	public NavigableSet<Double> getScores(){
		return mapByScore.descendingKeySet();
	}
	
	public ArrayList<String> getCookiesWithHighScores(int n){
		ArrayList<String> ret = new ArrayList<String>();
		TreeMap<String, Double> cookiesWithScores;
		int i=0;
		NavigableSet<Double> highScores = getScores();
		//System.out.println("SIZE: "+highScores.size());
		for(Double key : highScores){
			//System.out.println("KEY: "+key);
		//for(Map.Entry<Double, TreeMap<String, Double>> k : mapByScore.entrySet()){
			cookiesWithScores=get(key);
			//System.out.println("key: "+key);
			for(Map.Entry<String,Double> entr : cookiesWithScores.entrySet())
			{
				if((n!=0) && (i>=n)){
					break;
				}
				//System.out.println("i: "+i+" n: "+n);
				//System.out.println("Pridavam pouzivatela: "+entr.getKey()+" a jeho score je: "+entr.getValue());
				ret.add(entr.getKey() );
				i++;
			}
		}
		return ret;
	}
	
	
	public void updateUserScore(String s, Double n){
		Double sc;
		Double scr=-1.0;
		TreeMap<String, Double> tm; 
		if((sc=mapByCookie.get(s))==null)
			return;
		try{
			scr=mapByCookie.get(s);
			//System.out.println("Najdene: "+mapByScore.get(scr).get(s));
			mapByScore.get(scr).remove(s);
		}
		catch(NullPointerException e){
			System.out.println("Vynimka: "+e.toString()+" S: "+s+" N: "+n+" SCR: "+scr);
			//e.printStackTrace();
			return;
		}
		mapByCookie.remove(s);
		mapByCookie.put(s, n);
		
		//System.out.println("S: "+s+" - N: "+n);
		if((tm=mapByScore.get(n))==null){
			tm = new TreeMap<String, Double>();
			tm.put(s, n);
			mapByScore.put(n, tm);
			
		}
		else{
			mapByScore.get(n).put(s, n);
		}
	}
	
	
	
	
	public Double jaccardScoreUser(String s, HashSet<Long> originalUserArticles){
		HashSet<Long> targetUserArticles;
		Double jaccard=0.0;
		int intersection=0;
		int union=0;
		if((s==null) || s.equals(null))
			return null;
		if(get(s)!=0.0){
			return get(s);
		}
		
		targetUserArticles = MainClass.userMap2.get(s).articles;
		for(Long l : originalUserArticles){
			if(targetUserArticles.contains(l)){
				intersection++;
			}
		}
		union=originalUserArticles.size()+ targetUserArticles.size()-intersection;
		jaccard=(double)intersection/(double)union;
		//System.out.println("jaccard: Pridavam pouzivatela: "+s+" a jeho score je: "+jaccard);
		updateUserScore(s, jaccard);
		return jaccard;
	}
	
	
}
