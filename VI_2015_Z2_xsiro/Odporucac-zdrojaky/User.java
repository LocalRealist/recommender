package vi_zad2v1;

import java.util.ArrayList;

public class User {

	public String cookie;
	public ArrayList<String> keywords = new ArrayList<String>();
	public ArrayList<String> categories = new ArrayList<String>();
	public ArrayList<Long> articles = new ArrayList<Long>();
}
