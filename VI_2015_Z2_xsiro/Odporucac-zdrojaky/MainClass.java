package vi_zad2v1;
import vi_zad2v1.MySQLConnector;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import vi_zad2v1.DocumentScoreMap;



public class MainClass {

	public static String databaseName = "dat1";
	public static String username = "vi_user";
	public static String password = "password";	
	public static String outputFilePath="E:\\Dokumenty\\Studium\\FIIT\\FIIT\\VINF\\zad2\\Output\\the-file-name.txt";
	public static int pocetOdporucani=2000;
	  
	 public static TreeMap<String, HashMap<Long, Document>> docMap = new TreeMap<String, HashMap<Long, Document>>();
	  public static TreeMap<String, User> userMap = new TreeMap<String, User>();
	  public static TreeMap<String, User2> userMap2 = new TreeMap<String, User2>();
	  public static TreeMap<Long, Document2> docMap2 = new TreeMap<Long, Document2>();
	public static UserScoreMap userScoreMap = new UserScoreMap();
	public static DocumentScoreMap documentScoreMap = new DocumentScoreMap(); 
	public static PrintWriter writer;
	
	public static void main(String[] args) throws Exception
	{
		int safetyCounter=0;
		int safetyLimit=pocetOdporucani/2;
		HashSet<String> usedCookies = new HashSet<String>();
		MySQLConnector dao = new MySQLConnector();
		System.out.println("Toto je odporucac");
		try{
			writer = new PrintWriter(outputFilePath, "UTF-8");
		}
		catch(Exception e){
			System.out.println("Nepodarilo sa vytvorit zapisovac: "+e.toString());
			e.printStackTrace();
		}
		
		
		 Date date = new Date();
		 SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		 String formattedDate = sdf.format(date);
		 System.out.println("Dateum1: "+formattedDate);
		 
		 
		 
		 try{
			 dao.initDataBase();
			 dao.readDataBase();
			 //dao.createKeyWords(dao.readDataBase());
		 }
		 catch(Exception e){
			 System.out.println("Nastala vynimka, program konci: "+e.toString());
		 }
		 date = new Date();
		 formattedDate = sdf.format(date);
		 System.out.println("Datum2: "+formattedDate);
		 //System.in.read();
		 date = new Date();
		 formattedDate = sdf.format(date);
		 System.out.println("Datum 3: "+formattedDate);
	
		 
		 System.out.println("Pocet pouzivatelov: "+userMap2.size());
		 System.out.println("Pocet clankov: "+docMap2.size());
		 
		 
		 for(String testCookie : userMap2.descendingKeySet()){
			 if(usedCookies.contains(testCookie)){
				 continue;
			 }
			 usedCookies.add(testCookie);
			 if(userMap2.get(testCookie).articles.size()>2)
			 	dao.recommendToFile(testCookie);
			 else
				 continue;
			 safetyCounter++;
			 if(safetyCounter>=safetyLimit){
				 break;
			 }
			 if(safetyCounter%100==0){
				 date = new Date();
				 formattedDate = sdf.format(date);
				 System.out.println("Pocet vytvorenych odporucani: "+safetyCounter+"/"+pocetOdporucani+" ("+formattedDate+")");
			 }
		 }
		 
		 safetyCounter=0;
		 for(String testCookie : userMap2.keySet()){
			 if(usedCookies.contains(testCookie)){
				 continue;
			 }
			 usedCookies.add(testCookie);
			 if(userMap2.get(testCookie).articles.size()>2)
			 	dao.recommendToFile(testCookie);
			 else
				 continue;
			 safetyCounter++;
			 if(safetyCounter>=safetyLimit){
				 break;
			 }
			 if(safetyCounter%100==0){
				 date = new Date();
				 formattedDate = sdf.format(date);
				 System.out.println("Pocet vytvorenych odporucani: "+(safetyCounter+1000)+"/"+pocetOdporucani+" ("+formattedDate+")");
			 }
		 }
		 
		// dao.recommendToFile("12434421901251218");
		 System.out.println("Hotovo!");
		 writer.close();
		 dao.close();
		 	//	dao.initDataBase();
		
	}
	
	
	
	
	
	
	
	
}
