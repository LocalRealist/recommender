package vi_zad2v1;

import java.awt.List;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Arrays;

import vi_zad2v1.Document;
import vi_zad2v1.User;
import vi_zad2v1.DocumentMap;
import vi_zad2v1.Document2;
import vi_zad2v1.User2;
import vi_zad2v1.UserScore;
import vi_zad2v1.UserScoreMap;



public class MySQLConnector {
	private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  
	  
	  public String ckk = "12389484810144612";
	 ///TreeMap<>
	  public HashSet<String> ignoreWords = new HashSet<String>(Arrays.asList("pred", "vela", "boli", "budu", "dali", "daju", "nich", "mozu", "alebo", "potom", "shooty", "predtym"));
	  public HashSet<String> shortWords = new HashSet<String>(Arrays.asList("pc", "it", "is", "as", "eu", "sr", "sro", "usa", "stv", "stb", "kgb", "cia", "nsa", "kdh", "sas", "sns", "smk", "bmw", "kia", "omv", "vub", "hdp","lod", "usb", "lcd", "vlc", "vlk", "oko", "oci", "usi", "fit", "ztp"));
	  //public DocumentMap docMap = new DocumentMap();
	  public HashSet<String> trainCookies = new HashSet<String>();
	  public HashSet<String> testCookies = new HashSet<String>();
	  
	  //public TreeMap<String, HashMap<Long, Document>> docMap = new TreeMap<String, HashMap<Long, Document>>();
	  //public TreeMap<String, User> userMap = new TreeMap<String, User>();
	  //public TreeMap<String, User2> userMap2 = new TreeMap<String, User2>();
	  //public TreeMap<Long, Document2> docMap2 = new TreeMap<Long, Document2>();
	  //public TreeMap<Integer, UserScore> scoredUsers2 = new TreeMap<Integer, UserScore>();
	  //public TreeMap<String, Integer> scoredUsers3 = new TreeMap<String, Integer>();
	  
	  
	  //public HashSet<UserScore> scoredUsers = new HashSet<UserScore>();
	  //public HashSet<UserScore> top20 = new HashSet<UserScore>();
	  //public int minFromTop20=0;
	  
	  
	  public void initDataBase() throws Exception{
		  Class.forName("com.mysql.jdbc.Driver");
		  
		  connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/"+MainClass.databaseName+"?"
		              + "user="+MainClass.username+"&password="+MainClass.password+"");
		  
		  try{
			 
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  finally {
			      close();
			    }
		 
		  
		 // statement=connect.createStatement();
	  }
	  
	  
	  
	  
	
	public ResultSet readDataBase() throws Exception {
	    try {
	    	String s;
	    	int commonCookies=0;
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/"+MainClass.databaseName+"?"
		              + "user="+MainClass.username+"&password="+MainClass.password);

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query

	      System.out.println("jedna");
	      
	      resultSet=statement.executeQuery("SELECT url, sme_id, cookie FROM dat1.vi_train");
	      //resultSet=statement.executeQuery("SELECT * FROM dat1.vi_train");
	      // resultSet=statement.executeQuery("SELECT dat1.vi_test.url,dat1.vi_train.url from dat1.vi_test JOIN dat1.vi_train ON dat1.vi_test.cookie=dat1.vi_train.cookie WHERE dat1.vi_test.cookie="+ck+" AND dat1.vi_train.cookie="+ck+" AND dat1.vi_test.url=dat1.vi_train.url");
	      
	      System.out.println("dva");
	      //writeMetaData(resultSet);
	      System.out.println("tri");
	      //createKeyWords(resultSet);
	      createMatrix(resultSet);
	      //writeResultSet(resultSet);
	      System.out.println("styri");
	      
	    } catch (Exception e) {
	      throw e;
	    } finally {
	      close();
	    }
	    return resultSet;
	  }
	
	
	private void writeMetaData(ResultSet resultSet) throws SQLException {
	    
	    System.out.println("The columns in the table are: ");
	    
	    System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
	    for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
	      System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
	    }
	  }
	
	
	private void writeResultSet(ResultSet resultSet) throws SQLException {
		int i=1;
		String r;
	    while (resultSet.next()) {
	    	try{
	    			while(true)
	    			{
	    				r=resultSet.getString(i);
	    				i++;
	    				System.out.println(r);
	    			}
	    	
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println("======");
	    		i=1;
	    		continue;
	    	}
	    	
	    }
	  }
	
	public void createKeyWords(ResultSet resultSet) throws SQLException{
		int i=0;
		int j=0;
		int key=1;
		int ind=-1;
		int isHttps=0;
		boolean pr;
		long id=0;
		long sme_id=0;
		String r;
		String title="";
		String category="";
		String cookie="";
		Document doc;
		User user;
		ArrayList<String> keywords = new ArrayList<String>();
		ArrayList<String> auKW = new ArrayList<String>();
		HashMap<Long, Document> docs;
		
	    while (resultSet.next()) {
	    	j++;
	    	
	    	if(preparedStatement!=null){
	    		preparedStatement.close();
	    		//System.out.println("uzatvaram statement");
	    	}
	    	//if(j==4000){ return;}
	    	try{
	    				title="";
	    				cookie="";
	    				isHttps=0;
	    				pr=false;
	    				//id=resultSet.getLong(1);
	    				r=resultSet.getString(1);
	    				sme_id=resultSet.getLong(2);
	    				cookie=resultSet.getString(3);
	    				try{
		    				ind=r.indexOf("http://");
		    				if(ind==-1){
		    					ind=r.indexOf("https://");
		    					if(ind==-1)
		    						continue;
		    					isHttps=1;
		    				}
		    				category=r.substring(ind+7+isHttps, r.indexOf('.'));
		    				//System.out.println("category: "+category);
		    				//if(category.equals("primar") && !r.substring(r.length()-5, r.length()).equals(".html") ){		    				
			    			if(category.equals("primar") && (r.lastIndexOf('/')==r.length()-1 )){		    				
		    					pr=true;
		    					title="";
		    					auKW=new ArrayList<String>(Arrays.asList(r.split("/")));
		    					keywords=new ArrayList<String>(Arrays.asList(auKW.get(auKW.size()-1).split("-")));
		    					//System.out.println("Stranka primar: "+auKW.get(auKW.size()-1)+"----"+r);
		    					//keywords.clear();
		    				}
		    				else{
		    					keywords=new ArrayList<String>(Arrays.asList(r.substring(r.lastIndexOf('/')+1, r.lastIndexOf('.')).split("-")));
		    				}
	    				}catch(Exception e){
	    					//System.out.println(j+": sme_id: "+sme_id+": chyba pri parsovani: "+r+" .. "+e.toString());
	    					System.out.println("ch: "+j);
	    					continue;
	    				}
	    				
	    				if((user=MainClass.userMap.get(cookie))==null){
	    					user = new User();
	    					user.cookie=cookie;
	    					MainClass.userMap.put(cookie, user);
	    				}
	    				
	    				user.categories.add(category);
	    				user.articles.add(new Long(sme_id));
	    				
	    				doc= new Document();
	    				doc.category=category;
	    				doc.sme_id=sme_id;
	    				doc.keywords="";
	    				//doc.score=0;
	    				for(String word : keywords){
	    					if(isWord(word)){
	    						title+=" "+word;
	    						user.keywords.add(word);
	    						doc.keywords+=" "+word;
	    						if(MainClass.docMap.get(word)==null){
	    							docs=new HashMap<Long, Document>();
	    							docs.put(new Long(sme_id),  doc);
	    							MainClass.docMap.put(word, docs);
	    						}
	    						else{
	    							MainClass.docMap.get(word).put(new Long(sme_id), doc);
	    						}
	    						
	    						
	    					}
	    				}
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println("----Exception");
	    		e.printStackTrace();
	    	}
	    	//System.out.println("---------");
	    }
	}
	
	
	
	
	
	
	public void createMatrix(ResultSet resultSet) throws SQLException{
		int i=0;
		int j=0;
		int key=1;
		int ind=-1;
		int isHttps=0;
		boolean pr;
		long id=0;
		long sme_id=0;
		String r;
		String title="";
		String category="";
		String cookie="";
		Document2 doc;
		User2 user;
		HashSet<String> keywords = new HashSet<String>();
		ArrayList<String> auKW = new ArrayList<String>();
		HashMap<Long, Document2> docs;
		
	    while (resultSet.next()) {
	    	j++;
	    	
	    	if(preparedStatement!=null){
	    		preparedStatement.close();
	    		//System.out.println("uzatvaram statement");
	    	}
	    	//if(j==4000){ return;}
	    	try{
	    				title="";
	    				cookie="";
	    				isHttps=0;
	    				pr=false;
	    				//id=resultSet.getLong(1);
	    				r=resultSet.getString(1);
	    				sme_id=resultSet.getLong(2);
	    				cookie=resultSet.getString(3);
	    				try{
		    				ind=r.indexOf("http://");
		    				if(ind==-1){
		    					ind=r.indexOf("https://");
		    					if(ind==-1)
		    						continue;
		    					isHttps=1;
		    				}
		    				category=r.substring(ind+7+isHttps, r.indexOf('.'));
		    				//System.out.println("category: "+category);
		    				//if(category.equals("primar") && !r.substring(r.length()-5, r.length()).equals(".html") ){		    				
			    			if(category.equals("primar") && (r.lastIndexOf('/')==r.length()-1 )){		    				
		    					pr=true;
		    					title="";
		    					auKW=new ArrayList<String>(Arrays.asList(r.split("/")));
		    					keywords=new HashSet<String>(Arrays.asList(auKW.get(auKW.size()-1).split("-")));
		    					//System.out.println("Stranka primar: "+auKW.get(auKW.size()-1)+"----"+r);
		    					//keywords.clear();
		    				}
		    				else{
		    					keywords=new HashSet<String>(Arrays.asList(r.substring(r.lastIndexOf('/')+1, r.lastIndexOf('.')).split("-")));
		    				}
			    			
	    				}catch(Exception e){
	    					//System.out.println(j+": sme_id: "+sme_id+": chyba pri parsovani: "+r+" .. "+e.toString());
	    					//System.out.println("ch: "+j);
	    					continue;
	    				}
	    				
	    				if((user=MainClass.userMap2.get(cookie))==null){
	    					user = new User2();
	    					user.cookie=cookie;
	    					MainClass.userMap2.put(cookie, user);
	    				}
	    				
	    				user.categories.add(category);
	    				user.articles.add(new Long(sme_id));
	    				
	    				if((doc=MainClass.docMap2.get(sme_id))==null){
		    				doc=new Document2();
		    				doc.category=category;
		    				doc.sme_id=sme_id;
		    				doc.cookies.add(cookie);
		    				for(String word : keywords){
		    					if(isWord(word)){
		    						doc.keywords.add(word);
		    					}
		    				}
		    				//doc.keywords=keywords;
		    				MainClass.docMap2.put(sme_id, doc);
	    				}
	    				else{
	    					doc.cookies.add(cookie);
	    				}
	    				//doc.score=0;
	    				
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println("----Exception");
	    		e.printStackTrace();
	    	}
	    	//System.out.println("---------");
	    }
	}
	
	
	
	
	private boolean isWord(String word){
		if(word.length()<=3){
			return shortWords.contains(word);
		}
		else{
			return !ignoreWords.contains(word);
		}
	}

	public void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
	
	public void printDoc(String key){
		Document doc;
		for(Map.Entry<Long,Document> entry : MainClass.docMap.get(key).entrySet()){
			doc=entry.getValue();
			System.out.println("ID: "+doc.sme_id+", keywords: "+doc.keywords);
		}
	}
	public void printSmeIDs(String cookie){
		User2 user;
		user=MainClass.userMap2.get(cookie);
		for(Long smeId : user.articles ){
			System.out.println("sme_id: "+smeId);
		}
	}
	
	public void printRecommendations(String cookie){
		TreeMap<Long, Double> recommendations;
		recommendations = recommend(cookie);
		 Date date = new Date();
		 SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		 String formattedDate = sdf.format(date);
		 
		 System.out.println("Datum pre odporucaniami: "+formattedDate);
		 
		 
		for(Map.Entry<Long, Double> ent : recommendations.entrySet()){
			System.out.println("Recommendation ("+cookie+"): "+ent.getKey()+" Score: "+ent.getValue());
		}
		System.out.println("========================---========================");
		date = new Date();
		formattedDate = sdf.format(date);
		System.out.println("Datum po odporucaniach: "+formattedDate);
	}
	
	public ArrayList<String> getMostSimilarUsers(String cookie){
		ArrayList<String> ret = new ArrayList<String>();
		User2 user;
		Double userScore;
		Document2 doc;
		MainClass.userScoreMap = new UserScoreMap();
		user=MainClass.userMap2.get(cookie);
		for(Long smeId : user.articles ){
			doc = MainClass.docMap2.get(smeId);
			for(String ck : doc.cookies){
				//ret.add(ck);
				if(ck.equals(cookie))
					continue;
				if((userScore = MainClass.userScoreMap.get(ck))==null){
					userScore=0.0;
					//System.out.println("####pridavam usera, cookie pre "+smeId+": "+ck+" -- doc.cookies.size()="+doc.cookies.size());

					MainClass.userScoreMap.put(ck, userScore);
				}
				MainClass.userScoreMap.jaccardScoreUser(ck, user.articles);
			}
		}
	//	for(Map.Entry<String,Double> entry : MainClass.userScoreMap.mapByCookie. entrySet()){
	//		ret.add(entry.getKey());
		//System.out.println("***************");
		ret=MainClass.userScoreMap.getCookiesWithHighScores(30);
		//System.out.println("***************");
		
		return ret;
	}
	
	public TreeMap<Long, Double> getMostSimilarArticles(HashSet<Long> originalArticles, HashSet<Long> otherArticles){
		ArrayList<String> originalWords = new ArrayList<String>();
		TreeMap<Long, Double> ret = new TreeMap<Long, Double>();
		User2 user;
		Double docScore;
		Document2 doc;
		MainClass.documentScoreMap = new DocumentScoreMap();
		//System.out.print("Original words: ");
		for(Long l : originalArticles){
			for(String w : MainClass.docMap2.get(l).keywords ){
				originalWords.add(w);
			//	System.out.print(w+" ");
			}
			if(!MainClass.docMap2.get(l).category.equals("www"))
				originalWords.add(MainClass.docMap2.get(l).category);
		}
		for(Long smeId : otherArticles ){
			doc = MainClass.docMap2.get(smeId);
				if((docScore = MainClass.documentScoreMap.get(smeId))==null){
					docScore=0.0;
					MainClass.documentScoreMap.put(smeId, docScore);
				}
				
				
				MainClass.documentScoreMap.jaccardScoreDocument(smeId, originalWords);
			
		}
		ret=MainClass.documentScoreMap.getSmeIDsWithHighScores(40);
		
		return ret;
	}
	
	
	
	
	public TreeMap<Long, Double> recommend(String cookie){
		TreeMap<Long, Double> ret = new TreeMap<Long, Double>();
		ArrayList<String> users = new ArrayList<String>();
		TreeMap<Long, Double> similarArticles = new TreeMap<Long, Double>();
		String us;
		int i=0;
		users=getMostSimilarUsers(cookie);
		
		for(int a=0;a<users.size();a++){
			us = users.get(a);
			similarArticles = getMostSimilarArticles(MainClass.userMap2.get(cookie).articles, MainClass.userMap2.get(us).articles);
			HashSet<Long> original = MainClass.userMap2.get(cookie).articles;
			for(Long l : similarArticles.descendingKeySet()){
				if(i>=10){
					return ret;
					//break;
				}
				if(!original.contains(l)){
					//System.out.println("Odporucam clanok pre ("+cookie+"): "+l+" i="+i);
					ret.put(l, similarArticles.get(l));
					i++;
				}
				
				
			}
		}
		return ret;
	}
	
	
	public void recommendToFile(String cookie){
		TreeMap<Long, Double> recommendations;
		recommendations = recommend(cookie);
		
		for(Map.Entry<Long, Double> ent : recommendations.entrySet()){
			MainClass.writer.println(cookie+","+ent.getKey());
		}
		
	}
	
	
	

}
