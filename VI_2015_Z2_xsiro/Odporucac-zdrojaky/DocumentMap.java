package vi_zad2v1;

import vi_zad2v1.Document;

import java.util.ArrayList;
import java.util.TreeMap;

public class DocumentMap {
	private TreeMap<Long, Document> mapBySmeID = new TreeMap<Long, Document>();
	private TreeMap<String, Document> mapByKeyWords = new TreeMap<String, Document>(); 
	private TreeMap<String, ArrayList<Document>> mapByCategory = new TreeMap<String, ArrayList<Document>>();
	
	
	public int insert(Document value){
		try{
			mapBySmeID.put(value.sme_id, value);
			mapByKeyWords.put(value.keywords, value);
			mapByCategory.get(value.category).add(value);
		}
		catch(Exception e){
			System.out.println(e.toString());
			e.printStackTrace();
			return 1;
		}
		return 0;
	}
	
	public int insert(Long key, String keyName, Document value){
		if(keyName.equals("sme_id")){
			try{
				mapBySmeID.put(key, value);
				mapByKeyWords.put(value.keywords, value);
				mapByCategory.get(value.category).add(value);
			}
			catch(Exception e){
				System.out.println(e.toString());
				e.printStackTrace();
				return 2;
			}
		}
		else{
			System.out.println("Nespravny typ kluca: zadane:"+keyName+" -- ma byt: sme_id -- kluc: "+key);
			return 1;
		}
		return 0;
	}
	
	public int insert(String key, String keyName, Document value){
		if(keyName.equals("keywords")){
			try{
				mapBySmeID.put(value.sme_id, value);
				mapByKeyWords.put(key, value);
				mapByCategory.get(value.category).add(value);
			}
			catch(Exception e){
				System.out.println(e.toString());
				e.printStackTrace();
				return 2;
			}
		}
		else if(keyName.equals("category")){
			try{
				mapBySmeID.put(value.sme_id, value);
				mapByKeyWords.put(value.keywords, value);
				mapByCategory.get(key).add(value);
			}
			catch(Exception e){
				System.out.println(e.toString());
				e.printStackTrace();
				return 3;
			}
		}
		else{
			System.out.println("Nespravny typ kluca: zadane:"+keyName+" -- ma byt: keywords alebo category -- kluc: "+key);
			return 1;
		}
		return 0;
	}
	
	
	
	
	public Document get(Long key, String keyName){
		return mapBySmeID.get(key);
	}
	public Document get(Long key){
		return mapBySmeID.get(key);
	}
	
	public Document get(String key, String keyName){
		if(keyName.equals("keywords")){
			return mapByKeyWords.get(key);
		}
/*		else if(keyName.equals("category")){
			return mapByCategory.get(key);
		}
*/
		else return null;			
	}
	public ArrayList<Document> getCategory(String key, String keyName){
		return mapByCategory.get(key);
	}
	
	public Document getByCategory(String key, String keyName, int n){
		return mapByCategory.get(key).get(n);
	}
	
	
	public void printDoc(Long key, String keyName){
		Document doc =mapBySmeID.get(key);
		System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);

	}
	public void printDoc(Long key){
		Document doc = mapBySmeID.get(key);
		System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
	}
	
	public void printDoc(String key, String keyName){
		Document doc;
		if(keyName.equals("keywords")){
			doc= mapByKeyWords.get(key);
			System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
			return;
		}
		else return;
	}
/*		else if(keyName.equals("category")){
			doc= mapByCategory.get(key);
			System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
		}
*/
				
	public void printDoc(String key){
		Document doc;
		doc= mapByKeyWords.get(key);
		System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
		return;
	}
	public void printDoc(String key, int n){
		Document doc;
		doc= mapByCategory.get(key).get(n);
		System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
		return;
	}
/*	public void printDoc(Long key, String keyName){
		Document doc =mapBySmeID.get(key);
		System.out.println("sme_id: "+doc.sme_id+" -- category: "+doc.category+" -- keywords: "+doc.keywords);
	}
	*/
}
