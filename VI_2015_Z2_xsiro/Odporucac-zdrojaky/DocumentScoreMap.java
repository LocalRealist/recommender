package vi_zad2v1;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.ArrayList;

public class DocumentScoreMap {
	public TreeMap<Double, TreeMap<Long, Double>> mapByScore = new TreeMap<Double, TreeMap<Long, Double>>();
	public TreeMap<Long, Double> MapBySmeId = new TreeMap<Long, Double>();
	
	public TreeMap<Long, Double> get(Double key){
		return mapByScore.get(key);
	}
	
	public Double get(Long key){
		return MapBySmeId.get(key);
	}
	public void put(Double i, Long s){
		TreeMap<Long, Double> al; 
		//System.out.println("i pod n: "+i);
		if(mapByScore.get(i)==null){
			al = new TreeMap<Long, Double>();
			al.put(s, i);
			mapByScore.put(i, al);
		}
		else{
			mapByScore.get(i).put(s, i);
		}
		MapBySmeId.put(s, i);
	}
	
	public void put(Long s, Double i){
		TreeMap<Long, Double> al; 
		MapBySmeId.put(s, i);
		if(mapByScore.get(i)==null){
			
			al = new TreeMap<Long, Double>();
			al.put(s, i);
			mapByScore.put(i, al);
		}
		else{
			mapByScore.get(i).put(s, i);
		}
	}
	
	public NavigableSet<Double> getScores(){
		return mapByScore.descendingKeySet();
	}
	
	public TreeMap<Long, Double> getSmeIDsWithHighScores(int n){
		TreeMap<Long, Double> ret = new TreeMap<Long, Double>();
		TreeMap<Long, Double> smeIDsWithScores;
		String words="";
		int i=0;
		NavigableSet<Double> highScores = getScores();
		for(Double key : highScores){
			smeIDsWithScores=get(key);
			for(Map.Entry<Long,Double> entr : smeIDsWithScores.entrySet())
			{
				if((n!=0) && (i>=n)){
					break;
				}
				for(String w : MainClass.docMap2.get(entr.getKey()).keywords){
					words+=w+" ";
				}
				ret.put(entr.getKey(), entr.getValue() );
				i++;
			}
		}
		return ret;
	}
	
	
	public void updateDocumentScore(Long s, Double n){
		Double sc;
		TreeMap<Long, Double> tm; 
		if((sc=MapBySmeId.get(s))==null)
			return;
		try{
		mapByScore.get(MapBySmeId.get(s)).remove(s);
		}
		catch(NullPointerException e){
			System.out.println("Vynimka: "+e.toString());
			return;
		}
		MapBySmeId.remove(s);
		MapBySmeId.put(s, n);
		if((tm=mapByScore.get(n))==null){
			tm = new TreeMap<Long, Double>();
			tm.put(s, n);
			mapByScore.put(n, tm);
		}
		else{
			mapByScore.get(n).put(s, n);
		}
	}
	
	
	
	
	public Double jaccardScoreDocument(Long sId, ArrayList<String> originalUserWords){
		HashSet<String> targetArticleWords = new HashSet<String>();
		Double jaccard=0.0;
		int intersection=0;
		int union=0;
		if((sId==null) || sId.equals(null))
			return null;
		if(get(sId)!=0.0){
			return get(sId);
		}
		for(String st : MainClass.docMap2.get(sId).keywords){
			targetArticleWords.add(st);
		}
		if(!MainClass.docMap2.get(sId).category.equals("www"))
			targetArticleWords.add(MainClass.docMap2.get(sId).category);
		for(String st : originalUserWords){
			if(targetArticleWords.contains(st)){
				intersection++;
			}
		}
		union=originalUserWords.size()+ targetArticleWords.size()-intersection;
		jaccard=(double)intersection/(double)union;
		updateDocumentScore(sId, jaccard);
		return jaccard;
	}
	
}
