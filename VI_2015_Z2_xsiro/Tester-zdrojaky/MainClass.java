package vi_zad2v1_tester;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeMap;

import vi_zad2v1_tester.MySQLConnector;
import vi_zad2v1_tester.Document2;
import vi_zad2v1_tester.DocumentScoreMap;
import vi_zad2v1_tester.User2;
import vi_zad2v1_tester.UserScoreMap;
import vi_zad2v1_tester.ndcgDoc;

public class MainClass {
	
	
	public static String databaseName = "dat1";
	public static String username = "vi_user";
	public static String password = "password";
	public static String inputFilePath="E:\\Dokumenty\\Studium\\FIIT\\FIIT\\VINF\\zad2\\Output\\the-file-name.txt";

	public static TreeMap<String, User2> userMap2 = new TreeMap<String, User2>();
	public static TreeMap<Long, Document2> docMap2 = new TreeMap<Long, Document2>();
	public static UserScoreMap userScoreMap = new UserScoreMap();
	public static DocumentScoreMap documentScoreMap = new DocumentScoreMap(); 
	public static PrintWriter writer;
	public static BufferedReader inputReader;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String inputLine;
		ArrayList<String> elements = new ArrayList<String>();
		HashSet<String> unUsedCookies = new HashSet<String>();
		String rightArticles;
		String cookie;
		String smeId;
		String actualCookie = "";
		Scanner keyboard = new Scanner(System.in);
		String cont="";
		Long realSmeID;
		int userI=0;
		int safetyCounter=0;
		int safetyLimit=1000;
		boolean localRelevant=false;
		double good=0;
		double lines=0;
		double bad=0;
		double invalidCookies=0;
		double invalidRecommendations=0;
		double totalNDCGScore=0.0;
		double actDCG=0.0;
		double actIDCG=0.0;
		double localNDCG=0.0;
		
		ndcgDoc actualDoc;
		ArrayList<ndcgDoc> testedDocs = new ArrayList<ndcgDoc>();
		ArrayList<ndcgDoc> sortedDocs = new ArrayList<ndcgDoc>();
		
	System.out.println("Toto je tester");
		MySQLConnector dao = new MySQLConnector();
		
		try{
			dao.initDataBase();
			dao.readDataBase();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		/*
		 for(String testCookie : userMap2.descendingKeySet()){
			 System.out.println(testCookie);
			 safetyCounter++;
			 if(safetyCounter>=safetyLimit)
				 break;
		 }
		if(1==1)
			return;
		*/
		//System.out.println("Zadajte prikaz (zadajte \'y\' pre vykonanie testu):");
		do{
		cont="";	
		lines=0;
		good=0.0;
		lines=0.0;
		bad=0.0;
		invalidCookies=0.0;
		invalidRecommendations=0.0;
			try{
				inputReader  = new BufferedReader(new FileReader(inputFilePath));
			}
			catch(Exception e){
				System.out.println(e.toString());
				e.printStackTrace();
			}
			try{
				while(true){
					rightArticles="";
					inputLine=inputReader.readLine();
					lines++;
					elements = new ArrayList<String>(Arrays.asList(inputLine.split(",")));
					cookie=elements.get(0);
					cookie=cookie.trim();
					smeId=elements.get(1);
					smeId=smeId.trim();
					realSmeID=Long.valueOf(smeId);
					
					if(!cookie.equals(actualCookie) && !testedDocs.isEmpty()){
						localRelevant=false;
						actDCG=0.0;
						actIDCG=0.0;
						localNDCG=0.0;
						for(int i=0;i<testedDocs.size();i++){
							if(testedDocs.get(i).rel==1){
								localRelevant=true;
							}
						}
						//localRelevant=true;
						
						actDCG=testedDocs.get(0).rel;
						if(testedDocs.get(0).rel==1){
							sortedDocs.add(testedDocs.get(0));
						}
						for(int i=1;i<testedDocs.size();i++){
							actDCG+=((double)testedDocs.get(i).rel)/(log2(i+1));
							if(localRelevant){
								//System.out.println("pridavam: actDCG: "+actDCG+" pridavam: "+((double)testedDocs.get(i).rel)/(log2(i+1)));
							}
							if(testedDocs.get(i).rel==1){
								sortedDocs.add(testedDocs.get(i));
							}
						}
						for(int i=0;i<testedDocs.size();i++){
							if(testedDocs.get(i).rel==0){
								sortedDocs.add(testedDocs.get(i));
							}
						}
						actIDCG=sortedDocs.get(0).rel;
						for(int i=1;i<sortedDocs.size();i++){
							actIDCG+=((double)sortedDocs.get(i).rel)/(log2(i+1));
							if(localRelevant){
								//System.out.println("pridavam: actIDCG: "+actIDCG+" pridavam: "+((double)sortedDocs.get(i).rel)/(log2(i+1)));
							}
						}
						if(actIDCG!=0)
							localNDCG=actDCG/actIDCG;
						else
							localNDCG=0;
						totalNDCGScore+=localNDCG;
						if(localRelevant)
							System.out.println("NDCG pre "+actualCookie+" je "+localNDCG+" dalsie hodnoty: actDCG: "+actDCG+" actIDCG: "+actIDCG+" totalNDCGScore: "+totalNDCGScore);
						actualCookie=cookie;
						testedDocs.clear();
						sortedDocs.clear();
					}
					
					//System.out.println("Cookie: "+cookie+" : smeId: "+smeId);
					
					
					
					if(userMap2.containsKey(cookie)){
						actualDoc = new ndcgDoc();
						actualDoc.smeId=realSmeID;
						if(userMap2.get(cookie).articles.contains(realSmeID)){
							System.out.println("Zasah!: Cookie: "+cookie+" smeId: "+smeId+"\n---------------");
							good+=1;
							actualDoc.rel=1;
						}
						else{
							for(Long l : userMap2.get(cookie).articles){
								rightArticles+=l+" ";
							}
							//System.out.println("Netrafil cookie: "+cookie+" smeId: "+smeId+" Malo byt: "+rightArticles+"\n------------");
							bad+=1;
							actualDoc.rel=0;
						}
						testedDocs.add(actualDoc);
						
					}
					else{
						invalidRecommendations+=1;
						if(!unUsedCookies.contains(cookie)){
							//System.out.println("Toto cookie sa nenachadza v testovacej tabulke: "+cookie+"\n---------------");
							unUsedCookies.add(cookie);
							invalidCookies+=1;
						}
					}
				}
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			
			if(!testedDocs.isEmpty()){
				actDCG=0.0;
				actIDCG=0.0;
				localNDCG=0.0;
				actDCG=testedDocs.get(0).rel;
				if(testedDocs.get(0).rel==1){
					sortedDocs.add(testedDocs.get(0));
				}
				for(int i=1;i<testedDocs.size();i++){
					actDCG+=((double)testedDocs.get(i).rel)/(log2(i+1));
					
					if(testedDocs.get(i).rel==1){
						sortedDocs.add(testedDocs.get(i));
					}
				}
				actIDCG=sortedDocs.get(0).rel;
				for(int i=0;i<sortedDocs.size();i++){
					actIDCG+=((double)testedDocs.get(i).rel)/(log2(i+1));
				}
				localNDCG=actDCG/actIDCG;
				System.out.println("NDCG pre "+actualCookie+" je "+localNDCG);
				//actualCookie=cookie;
				testedDocs.clear();
				sortedDocs.clear();
			}
			
			System.out.println("Toto je tester");
			System.out.println("riadky: "+lines);
			System.out.println("Uspesne odporucania: "+good);
			System.out.println("Neuspesne odporucania: "+bad);
			System.out.println("Cookies ktore sa nevyskytuju v testovacej mnozine: "+invalidCookies);
			System.out.println("Odporucania viazuce sa k tymto cookies: "+invalidRecommendations);
			System.out.println("Precision at n: "+good/(bad+good));
			System.out.println("Priemerna NDCG je "+totalNDCGScore/(good+bad));
			System.out.println("Zadajte prikaz (zadajte \'y\' pre vykonanie testu):");
		}while((cont=keyboard.nextLine()).equals("y"));
	}

	public static double log2(Double d){
		return Math.log(d)/Math.log(2);
	}
	public static double log2(Integer d){
		return Math.log(d)/Math.log(2);
	}
}
