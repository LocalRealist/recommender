package vi_zad2v1_tester;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import vi_zad2v1_tester.Document2;
import vi_zad2v1_tester.MainClass;
import vi_zad2v1_tester.User2;



public class MySQLConnector {
	private Connection connect = null;
	  private Statement statement = null;
	  private PreparedStatement preparedStatement = null;
	  private ResultSet resultSet = null;
	  
	  public String ckk = "12389484810144612";
	 ///TreeMap<>
	  //public ArrayList<String> ignoreWords = new ArrayList<String>(Arrays.asList("pred", "vela", "boli", "budu", "dali", "daju", "nich", "mozu", "alebo", "potom", "shooty", "predtym"));
	  //public ArrayList<String> shortWords = new ArrayList<String>(Arrays.asList("pc", "as", "eu", "sr", "sro", "usa", "stv", "stb", "kgb", "cia", "nsa", "kdh", "sas", "sns", "smk", "bmw", "kia", "omv", "vub", "hdp","lod", "usb", "lcd", "vlc", "vlk", "oko", "oci", "usi", "fit", "ztp"));
	  //public DocumentMap docMap = new DocumentMap();
	  public HashSet<String> trainCookies = new HashSet<String>();
	  public HashSet<String> testCookies = new HashSet<String>();

	  
	  
	  public void initDataBase() throws Exception{
		  Class.forName("com.mysql.jdbc.Driver");
		  
		  connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/"+MainClass.databaseName+"?"
		              + "user="+MainClass.username+"&password="+MainClass.password+"");
		  
		  try{
			  
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  finally {
			      close();
			    }
		 
	  }
	  
	  public void getCommonCookies() throws Exception{
		  try {
		    	String s;
		    	int commonCookies=0;
		      Class.forName("com.mysql.jdbc.Driver");
		      connect = DriverManager
		          .getConnection("jdbc:mysql://localhost/"+MainClass.databaseName+"?"
		              + "user="+MainClass.username+"&password="+MainClass.password);

		      statement = connect.createStatement();

		      System.out.println("jedna");
		      resultSet=statement.executeQuery("SELECT cookie FROM dat1.vi_train");
		      
		      while(resultSet.next()){
		    	  s=resultSet.getString(1);
		    	  trainCookies.add(s);
		      }
		      resultSet.close();
		      resultSet=statement.executeQuery("SELECT cookie FROM dat1.vi_test");
		      while(resultSet.next()){
		    	  s=resultSet.getString(1);
		    	  testCookies.add(s);
		      }
		      for(String st : trainCookies){
		    	  if(testCookies.contains(st)){
		    		  commonCookies++;
		    	  }
		      }
		      System.out.println("Pocet rovnakych cookies: "+commonCookies);
		  }
		  catch(Exception e){
			throw e;  
		  }
		  finally {
		      close();
		    }
		      	return;
	  }
	  
	  
	
	public ResultSet readDataBase() throws Exception {
	    try {
	    	String s;
	    	int commonCookies=0;
	      // This will load the MySQL driver, each DB has its own driver
	      Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
	      connect = DriverManager
	          .getConnection("jdbc:mysql://localhost/dat1?"
	              + "user=vi_user&password=password");

	      // Statements allow to issue SQL queries to the database
	      statement = connect.createStatement();
	      // Result set get the result of the SQL query

	      System.out.println("jedna");
	      
	      resultSet=statement.executeQuery("SELECT url, sme_id, cookie FROM dat1.vi_test");
	      //resultSet=statement.executeQuery("SELECT * FROM dat1.vi_train");
	      // resultSet=statement.executeQuery("SELECT dat1.vi_test.url,dat1.vi_train.url from dat1.vi_test JOIN dat1.vi_train ON dat1.vi_test.cookie=dat1.vi_train.cookie WHERE dat1.vi_test.cookie="+ck+" AND dat1.vi_train.cookie="+ck+" AND dat1.vi_test.url=dat1.vi_train.url");
	      
	      System.out.println("dva");
	      //writeMetaData(resultSet);
	      System.out.println("tri");
	      //createKeyWords(resultSet);
	      createMatrix(resultSet);
	      //writeResultSet(resultSet);
	      System.out.println("styri");
	      
	    } catch (Exception e) {
	      throw e;
	    } finally {
	      close();
	    }
	    return resultSet;
	  }
	
	
	public void createMatrix(ResultSet resultSet) throws SQLException{
		int i=0;
		int j=0;
		int key=1;
		int ind=-1;
		int isHttps=0;
		boolean pr;
		long id=0;
		long sme_id=0;
		String r;
		String title="";
		String category="";
		String cookie="";
		Document2 doc;
		User2 user;
		HashSet<String> keywords = new HashSet<String>();
		ArrayList<String> auKW = new ArrayList<String>();
		HashMap<Long, Document2> docs;
		
	    while (resultSet.next()) {
	    	j++;
	    	
	    	if(preparedStatement!=null){
	    		preparedStatement.close();
	    		//System.out.println("uzatvaram statement");
	    	}
	    	//if(j==4000){ return;}
	    	try{
	    				title="";
	    				cookie="";
	    				isHttps=0;
	    				pr=false;
	    				//id=resultSet.getLong(1);
	    				r=resultSet.getString(1);
	    				sme_id=resultSet.getLong(2);
	    				cookie=resultSet.getString(3);
	    				
	    				
	    				if((user=MainClass.userMap2.get(cookie))==null){
	    					user = new User2();
	    					user.cookie=cookie;
	    					MainClass.userMap2.put(cookie, user);
	    				}
	    				
	    				//user.categories.add(category);
	    				user.articles.add(new Long(sme_id));
	    				
	    				if((doc=MainClass.docMap2.get(sme_id))==null){
		    				doc=new Document2();
		    				//doc.category=category;
		    				doc.sme_id=sme_id;
		    				doc.cookies.add(cookie);
		    				/*
		    				for(String word : keywords){
		    					if(isWord(word)){
		    						doc.keywords.add(word);
		    					}
		    				}
		    				*/
		    				//doc.keywords=keywords;
		    				MainClass.docMap2.put(sme_id, doc);
	    				}
	    				else{
	    					doc.cookies.add(cookie);
	    				}
	    				//doc.score=0;
	    				
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println("----Exception");
	    		e.printStackTrace();
	    	}
	    	//System.out.println("---------");
	    }
	}
	
	public void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
